﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountService.Core;
using Microsoft.Extensions.DependencyInjection;

namespace AccountService.WebApi
{
    public class ServiceResolver : IServiceResolver
    {
        private readonly IServiceProvider _provider;

        public ServiceResolver(IServiceProvider provider)
        {
            _provider = provider;
        }

        public T Resolve<T>()
        {
            return _provider.GetService<T>();
        }
    }
}
