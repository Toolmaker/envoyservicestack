﻿using AccountService.Domain.Interfaces;
using System;

namespace AccountService.WebApi
{
    public class HardCodedAccountSettings : IAccountSettings
    {
        public int MaxNumberOfFailedAttempts => 3;
        public TimeSpan WrongPasswordTimeout => TimeSpan.FromMinutes(5);
        public TimeSpan LockoutDuration => TimeSpan.FromMinutes(15);
    }
}
