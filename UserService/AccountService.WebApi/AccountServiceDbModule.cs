﻿using AccountService.Persistence;
using AccountService.WebApi.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace AccountService.WebApi
{
    public class AccountServiceDbModule : IDependencyInjectionModule
    {
        public void Register(IServiceCollection services)
        {
            services.AddDbContext<AccountContext>(opts => opts.UseSqlite("Data Source=accounts.db"));
        }
    }
}