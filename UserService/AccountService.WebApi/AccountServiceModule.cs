﻿using AccountService.Application;
using AccountService.Application.Mappings;
using AccountService.Core;
using AccountService.Domain;
using AccountService.Domain.Interfaces;
using AccountService.Domain.Interfaces.Repositories;
using AccountService.Infrastructure;
using AccountService.Persistence;
using AccountService.Persistence.Mappings;
using AccountService.WebApi.Extensions;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace AccountService.WebApi
{
    public class AccountServiceModule : IDependencyInjectionModule
    {
        public void Register(IServiceCollection services)
        {
            services.AddTransient<ILoginService, LoginService>();
            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<IAccount, Account>();
            services.AddTransient<Account>();
            services.AddTransient<IHashPassword, Sha256Hasher>();
            services.AddTransient<ITimeProvider, TimeProvider>();
            services.AddTransient<IAccountSettings, HardCodedAccountSettings>();
            services.AddTransient<IServiceResolver, ServiceResolver>();

            services.AddTransient<IMapper, Mapper>();
            services.AddSingleton<IConfigurationProvider>(p => new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ApplicationProfile>();
                cfg.AddProfile<PersistenceProfile>();
                cfg.ConstructServicesUsing(p.GetService);
            }));
        }
    }
}
