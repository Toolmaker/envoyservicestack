﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountService.Application;
using AccountService.Application.DataTransfer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Optional.Unsafe;

namespace AccountService.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService _loginService;

        public LoginController(ILoginService loginService)
        {
            _loginService = loginService;
        }

        [HttpGet]
        public string Get()
        {
            return "Hello world";
        }

        // POST: api/Login
        [HttpPost]
        public ActionResult Post([FromBody] LoginModel loginModel)
        {
            var result = _loginService.Login(loginModel);
            if (!result.HasValue)
                return Unauthorized();

            return new JsonResult(result.ValueOrFailure());
        }
    }
}
