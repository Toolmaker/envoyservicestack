﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace AccountService.WebApi.Extensions
{
    public interface IDependencyInjectionModule
    {
        void Register(IServiceCollection services);
    }

    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddModule<T>(this IServiceCollection services) where T : IDependencyInjectionModule, new()
        {
            var module = new T();
            module.Register(services);

            return services;
        }
    }
}
