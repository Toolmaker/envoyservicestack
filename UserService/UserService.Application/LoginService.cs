﻿using System;
using AccountService.Application.DataTransfer;
using AccountService.Core;
using AccountService.Domain.Interfaces.Repositories;
using AutoMapper;
using Optional;
using Optional.Unsafe;

namespace AccountService.Application
{
    public interface ILoginService
    {
        Option<AccountModel> Login(LoginModel loginModel);
    }

    public class LoginService : ILoginService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IMapper _mapper;

        public LoginService(IAccountRepository accountRepository, IMapper mapper)
        {
            _accountRepository = accountRepository;
            _mapper = mapper;
        }

        public Option<AccountModel> Login(LoginModel loginModel)
        {
            if (loginModel == null) throw new ArgumentNullException(nameof(loginModel));

            var accountOption = _accountRepository.GetByEmailAddress(loginModel.Username);
            if (!accountOption.HasValue)
                return Option.None<AccountModel>();

            var account = accountOption.ValueOrFailure();
            if (!account.ValidatePassword(loginModel.Password))
                return Option.None<AccountModel>();

            return Option.Some(_mapper.Map<AccountModel>(account));
        }
    }
}
