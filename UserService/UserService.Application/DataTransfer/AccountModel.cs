﻿using System;

namespace AccountService.Application.DataTransfer
{
    public class AccountModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public DateTimeOffset LastLogin { get; set; }
        public string Token { get; set; }
    }
}
