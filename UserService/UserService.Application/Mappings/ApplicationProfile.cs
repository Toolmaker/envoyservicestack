﻿using AccountService.Application.DataTransfer;
using AccountService.Domain.Interfaces;
using AutoMapper;

namespace AccountService.Application.Mappings
{
    public class ApplicationProfile : Profile
    {
        public ApplicationProfile()
        {
            CreateMap<IAccount, AccountModel>();
        }
    }
}
