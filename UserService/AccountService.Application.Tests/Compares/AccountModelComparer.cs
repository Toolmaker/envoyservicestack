﻿using AccountService.Application.DataTransfer;
using System.Collections.Generic;

namespace AccountService.Application.Tests.Compares
{
    public class AccountModelComparer : IEqualityComparer<AccountModel>
    {
        public bool Equals(AccountModel x, AccountModel y)
        {
            return x.Email == y.Email &&
                   x.Name == y.Name &&
                   x.LastLogin == y.LastLogin &&
                   x.Token == y.Token &&
                   x.Role == y.Role;
        }

        public int GetHashCode(AccountModel obj)
        {
            return System.HashCode.Combine(obj.Email, obj.LastLogin, obj.Name, obj.Token);
        }
    }
}
