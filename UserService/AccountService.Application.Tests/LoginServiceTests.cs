﻿using System;
using AccountService.Application.DataTransfer;
using AccountService.Application.Mappings;
using AccountService.Application.Tests.Builders;
using AccountService.Application.Tests.Compares;
using AccountService.Domain;
using AccountService.Domain.Interfaces;
using AccountService.Domain.Interfaces.Repositories;
using AutoMapper;
using Moq;
using Optional;
using Xunit;

namespace AccountService.Application.Tests
{
    public class LoginServiceTests
    {
        private readonly string _expectedEmail = Guid.NewGuid().ToString();
        private readonly string _expectedName = Guid.NewGuid().ToString();

        private readonly IMapper _mapper;
        private readonly Mock<IAccountRepository> _accountRepository = new Mock<IAccountRepository>();
        private readonly DateTimeOffset _now = DateTimeOffset.Now;

        public LoginServiceTests()
        {
            _mapper = new MapperConfiguration(cfg => cfg.AddProfile<ApplicationProfile>()).CreateMapper();
        }

        private LoginService CreateService()
        {
            return new LoginService(_accountRepository.Object, _mapper);
        }

        private Mock<IAccount> CreateAccountWithValidatePassword(string expectedPassword, bool willSucceed)
        {
            return AccountMockBuilder
                .Create()
                .WithEmail(_expectedEmail)
                .WithName(_expectedName)
                .WithValidatePassword(expectedPassword, willSucceed)
                .WithLastLogon(_now)
                .WithRole(AccountRoles.Administrator)
                .Build();
        }

        private void SetupAccountRepositoryWithAccount(string expectedUsername, IAccount account)
        {
            _accountRepository
                .Setup(r => r.GetByEmailAddress(It.Is((string v) => v == expectedUsername)))
                .Returns(Option.Some(account));
        }

        [Fact]
        public void Login_WithNullLoginModel_ThrowsArgumentNullException()
        {
            var service = CreateService();

            Assert.Throws<ArgumentNullException>(() => service.Login(null));
        }

        [Fact]
        public void Login_WithMatchingAccountAndCorrectPassword_ReturnsTrue()
        {
            var expectedPassword = Guid.NewGuid().ToString();
            var accountMock = CreateAccountWithValidatePassword(expectedPassword, true);
            var expectedDto = new AccountModel {Email = _expectedEmail, LastLogin = _now, Name = _expectedName, Role = AccountRoles.Administrator.ToString() };
            SetupAccountRepositoryWithAccount(_expectedEmail, accountMock.Object);

            var service = CreateService();
            var actualResult = service.Login(new LoginModel()
            {
                Username = _expectedEmail,
                Password = expectedPassword
            }).ValueOr((AccountModel) null);

            Assert.Equal(expectedDto, actualResult, new AccountModelComparer());
        }

        [Fact]
        public void Login_WithWrongPassword_ReturnsNone()
        {
            var expectedPassword = Guid.NewGuid().ToString();
            var accountMock = CreateAccountWithValidatePassword(string.Empty, false);
            SetupAccountRepositoryWithAccount(_expectedEmail, accountMock.Object);

            var service = CreateService();
            Assert.Equal(Option.None<AccountModel>(), service.Login(new LoginModel()
            {
                Username = _expectedEmail,
                Password = expectedPassword
            }));
        }
    }
}
