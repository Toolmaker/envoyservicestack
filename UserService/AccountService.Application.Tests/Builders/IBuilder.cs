﻿namespace AccountService.Application.Tests.Builders
{
    interface IBuilder<T>
    {
        T Build();
    }
}
