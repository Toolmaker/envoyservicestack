﻿using AccountService.Domain.Interfaces;
using Moq;
using System;
using AccountService.Domain;

namespace AccountService.Application.Tests.Builders
{
    public class AccountMockBuilder : IBuilder<Mock<IAccount>>
    {
        private readonly Mock<IAccount> _mock = new Mock<IAccount>();

        private AccountMockBuilder()
        {
        }

        public static AccountMockBuilder Create()
        {
            return new AccountMockBuilder();
        }

        public AccountMockBuilder WithValidatePassword(string expectedPassword, bool returnValue)
        {
            _mock
                .Setup(m => m.ValidatePassword(It.Is((string p) => p == expectedPassword)))
                .Returns(returnValue);

            return this;
        }

        public AccountMockBuilder WithEmail(string email)
        {
            _mock.Setup(m => m.Email).Returns(email);

            return this;
        }

        public AccountMockBuilder WithLastLogon(DateTimeOffset now)
        {
            _mock.Setup(a => a.LastLogin).Returns(now);

            return this;
        }

        public AccountMockBuilder WithName(string name)
        {
            _mock.Setup(m => m.Name).Returns(name);

            return this;
        }

        public AccountMockBuilder WithRole(AccountRoles role)
        {
            _mock.Setup(a => a.Role).Returns(role);

            return this;
        }

        public Mock<IAccount> Build()
        {
            return _mock;
        }
    }
}
