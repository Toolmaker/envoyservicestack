﻿using AccountService.Application.DataTransfer;
using Microsoft.AspNetCore.Mvc.Formatters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Xunit;

namespace AccountService.WebApi.Tests.Controllers
{
    public class LoginControllerTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public LoginControllerTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async void Get_ReturnsHelloWorld()
        {
            var response = await _client.GetAsync("/api/Login");

            var content = await response.Content.ReadAsStringAsync();

            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async void Login_WithValidCredentials_ReturnsAccountModel()
        {
            var expectedModel = new LoginModel { Username = "patrick@80db.nl", Password = "password" };
            var content = new StringContent(JsonConvert.SerializeObject(expectedModel), Encoding.UTF8, "application/json");
            var response = await _client.PostAsync("/api/Login", content);

            response.EnsureSuccessStatusCode();

            var returnContent = await response.Content.ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<AccountModel>(returnContent);

            Assert.Equal(expectedModel.Username, model.Email);
        }
    }
}
