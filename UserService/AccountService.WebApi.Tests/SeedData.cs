﻿using AccountService.Persistence;
using System;
using System.Security.Cryptography;
using System.Text;

namespace AccountService.WebApi.Tests
{
    public class SeedData
    {
        public static void PopulateTestData(AccountContext db)
        {
            var password = Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes("password")));

            db.Accounts.Add(new AccountEntity()
            {
                Email = "patrick@80db.nl",
                Name = "Patrick Admin",
                Password = password,
                Role = 7,
                LastFailedLogon = null,
                LastLogin = DateTimeOffset.UnixEpoch,
                Locked = false,
                FailedLoginAttempts = 0,
                LockedUntil = null
            });

            db.Accounts.Add(new AccountEntity()
            {
                Email = "patrick@toolmaker.nl",
                Name = "Patrick User",
                Password = password,
                Role = 1,
                LastFailedLogon = null,
                LastLogin = DateTimeOffset.UnixEpoch,
                Locked = false,
                FailedLoginAttempts = 0,
                LockedUntil = null
            });

            db.SaveChanges();
        }
    }
}
