﻿namespace AccountService.Core
{
    public interface IHashPassword
    {
        string Hash(string input);
    }
}
