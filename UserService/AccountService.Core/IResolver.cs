﻿namespace AccountService.Core
{
    public interface IServiceResolver
    {
        T Resolve<T>();
    }
}
