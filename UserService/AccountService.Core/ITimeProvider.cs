﻿using System;

namespace AccountService.Core
{
    public interface ITimeProvider
    {
        DateTimeOffset Now { get; }
    }
}
