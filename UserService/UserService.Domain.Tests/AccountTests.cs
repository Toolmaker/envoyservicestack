﻿using AccountService.Domain.Interfaces;
using Moq;
using System;
using AccountService.Core;
using AccountService.Domain.Exceptions;
using Xunit;

namespace AccountService.Domain.Tests
{
    public class AccountTests
    {
        private readonly Mock<IHashPassword> _passwordHasher = new Mock<IHashPassword>();
        private readonly Mock<ITimeProvider> _timeProvider = new Mock<ITimeProvider>();
        private readonly Mock<IAccountSettings> _accountSettings = new Mock<IAccountSettings>();

        private readonly string _expectedEmail = "patrick@80db.nl";
        private readonly string _expectedName = "Patrick van der Willik";
        private readonly string _expectedPassword = Guid.NewGuid().ToString();
        private readonly TimeSpan _lockoutDuration = TimeSpan.FromMinutes(15);
        private readonly TimeSpan _wrongPasswordTimeout = TimeSpan.FromMinutes(5);

        private DateTimeOffset _now = DateTimeOffset.Now;
        private int _maxNumberOfFailedAttempts = 3;

        public AccountTests()
        {
            _passwordHasher
                .Setup(h => h.Hash(It.IsAny<string>()))
                .Returns((string v) => v);

            _timeProvider.Setup(p => p.Now)
                .Returns(_now);

            _accountSettings
                .Setup(s => s.LockoutDuration)
                .Returns(_lockoutDuration);

            _accountSettings
                .Setup(s => s.WrongPasswordTimeout)
                .Returns(_wrongPasswordTimeout);
        }

        private Account CreateAccount()
        {
            _accountSettings
                .Setup(s => s.MaxNumberOfFailedAttempts)
                .Returns(_maxNumberOfFailedAttempts);

            return new Account(_passwordHasher.Object, _timeProvider.Object, _accountSettings.Object)
            {
                Email = _expectedEmail,
                Name = _expectedName,
                Password = _expectedPassword,
                Role = AccountRoles.Administrator
            };
        }

        #region SetPassword

        [Fact]
        public void SetPassword_WithEmptyPassword_ThrowsInvalidPasswordException()
        {
            var account = CreateAccount();

            Assert.Throws<InvalidPasswordException>(() => account.SetPassword(string.Empty));
        }

        [Fact]
        public void SetPassword_WithNullPassword_ThrowsArgumentException()
        {
            var account = CreateAccount();

            Assert.Throws<ArgumentNullException>(() => account.SetPassword(null));
        }

        [Fact]
        public void SetPassword_WithProperPassword_UpdatesPassword()
        {
            var expectedPassword = Guid.NewGuid().ToString();
            var account = CreateAccount();

            account.SetPassword(expectedPassword);

            Assert.Equal(expectedPassword, account.Password);
            _passwordHasher.Verify(p => p.Hash(It.Is((string pass) => pass == expectedPassword)), Times.Once);
        }
        #endregion  


        #region ValidatePassword

        [Fact]
        public void ValidatePassword_WithCorrectPassword_ReturnsTrue()
        {
            var account = CreateAccount();

            Assert.True(account.ValidatePassword(_expectedPassword));
            _passwordHasher.Verify(h => h.Hash(It.Is((string v) => v == _expectedPassword)), Times.Once());
        }

        [Fact]
        public void ValidatePassword_WithIncorrectPassword_ReturnsFalse()
        {
            var actualPassword = Guid.NewGuid().ToString();
            var account = CreateAccount();

            Assert.False(account.ValidatePassword(actualPassword));
            _passwordHasher.Verify(h => h.Hash(It.Is((string v) => v == actualPassword)), Times.Once());
        }

        [Fact]
        public void ValidatePassword_WithCorrectPassword_WillUpdateLastLogonTime()
        {
            var account = CreateAccount();

            Assert.True(account.ValidatePassword(_expectedPassword));

            Assert.Equal(_now, account.LastLogin);
        }

        [Fact]
        public void ValidatePassword_WithWrongPassword_WillIncreaseFailedAttempts()
        {
            var account = CreateAccount();
            var currentFailedAttemptCount = account.FailedLoginAttempts;

            account.ValidatePassword(string.Empty);

            Assert.Equal(currentFailedAttemptCount + 1, account.FailedLoginAttempts);
        }

        [Fact]
        public void ValidatePassword_WithWrongPassword_WillSetLastFailedLoginAttemptToNow()
        {
            var account = CreateAccount();

            account.ValidatePassword(string.Empty);

            Assert.Equal(_now, account.LastFailedLogon);
        }

        [Fact]
        public void ValidatePassword_WithCorrectPasswordAfterFailedLogin_ResetFailedAttempts()
        {
            const int expectedFailureCount = 0;

            var account = CreateAccount();

            account.ValidatePassword(string.Empty);
            account.ValidatePassword(_expectedPassword);

            Assert.Equal(expectedFailureCount, account.FailedLoginAttempts);
        }

        [Fact]
        public void ValidatePassword_WithWrongPasswordAfterWrongPasswordTimeout_WillResetFailedAttempts()
        {
            const int expectedFailureCount = 1;

            var account = CreateAccount();
            account.ValidatePassword(string.Empty);
            account.ValidatePassword(string.Empty);
            _now = _now + _wrongPasswordTimeout;
            _timeProvider.Setup(p => p.Now).Returns(_now);

            account.ValidatePassword(string.Empty);

            Assert.Equal(expectedFailureCount, account.FailedLoginAttempts);
        }

        [Fact]
        public void ValidatePassword_WithMaximumNumberOfFailures_LocksAccount()
        {
            _maxNumberOfFailedAttempts = 1;

            var account = CreateAccount();
            account.ValidatePassword(string.Empty);

            Assert.True(account.Locked);
        }

        [Fact]
        public void ValidatePassword_WithMaximumNumberOfFailures_SetsLockedUntil()
        {
            var expectedLockedUntil = _now + _lockoutDuration;
            _maxNumberOfFailedAttempts = 1;

            var account = CreateAccount();

            account.ValidatePassword(string.Empty);

            Assert.True(account.Locked);
            Assert.Equal(expectedLockedUntil, account.LockedUntil);
        }

        [Fact]
        public void ValidatePassword_WithLockedAccount_WillAlwaysReturnFalse()
        {
            var account = CreateAccount();
            account.Lock();

            var actualResult = account.ValidatePassword(_expectedPassword);

            Assert.False(actualResult);
        }

        [Fact]
        public void ValidatePassword_WithLockedAccountAfterTimeout_WillValidatePassword()
        {
            var account = CreateAccount();
            account.Lock();
            _timeProvider
                .Setup(p => p.Now)
                .Returns(_now + _lockoutDuration);

            var actualResult = account.ValidatePassword(_expectedPassword);

            Assert.True(actualResult);
        }

        #endregion

        #region Lock

        [Fact]
        public void Lock_WithUnlockedAccount_LocksAccount()
        {
            var account = CreateAccount();

            account.Lock();

            Assert.True(account.Locked);
            Assert.Equal(_now + _lockoutDuration, account.LockedUntil);
        }

        [Fact]
        public void Lock_OnLockedACcount_WontResetLockedUntil()
        {
            var account = CreateAccount();
            account.Lock();
            var expectedLockedUntil = account.LockedUntil;

            _timeProvider
                .Setup(p => p.Now)
                .Returns(_now + TimeSpan.FromMinutes(1));

            account.Lock();

            Assert.True(account.Locked);
            Assert.Equal(_now + _lockoutDuration, account.LockedUntil);

        }
        #endregion

        #region Unlock

        [Fact]
        public void Unlock_OnLockedAccount_WillUnlock()
        {
            var account = CreateAccount();
            account.Lock();

            account.Unlock();

            Assert.False(account.Locked);
        }

        [Fact]
        public void Unlock_OnLockedAccount_WillUnsetLockedUntil()
        {
            var account = CreateAccount();
            account.Lock();

            account.Unlock();

            Assert.Equal(DateTimeOffset.MinValue, account.LockedUntil);
        }
        #endregion
    }
}
