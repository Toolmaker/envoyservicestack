﻿using System;
using System.Runtime.Serialization;

namespace AccountService.Domain.Exceptions
{
    public class InvalidPasswordException : Exception
    {
        public InvalidPasswordException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public InvalidPasswordException() : base()
        {
        }

        public InvalidPasswordException(string message) : base(message)
        {

        }
    }
}
