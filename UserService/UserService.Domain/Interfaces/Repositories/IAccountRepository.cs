﻿using Optional;

namespace AccountService.Domain.Interfaces.Repositories
{
    public interface IAccountRepository
    {
        Option<IAccount> GetByEmailAddress(string email);
    }
}
