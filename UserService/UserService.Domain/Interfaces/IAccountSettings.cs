﻿using System;

namespace AccountService.Domain.Interfaces
{
    public interface IAccountSettings
    {
        int MaxNumberOfFailedAttempts { get; }
        TimeSpan WrongPasswordTimeout { get; }
        TimeSpan LockoutDuration { get; }
    }
}
