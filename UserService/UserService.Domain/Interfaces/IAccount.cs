﻿using System;

namespace AccountService.Domain.Interfaces
{
    public interface IAccount
    {
        string Name { get;}
        string Email { get; }
        DateTimeOffset LastLogin { get;}
        AccountRoles Role { get; }
        void SetPassword(string password);
        bool ValidatePassword(string password);
        void Lock();
        void Unlock();
    }
}