﻿namespace AccountService.Domain
{
    public enum AccountRoles
    {
        None,
        Prospect,
        Client,
        Moderator,
        Broadcaster,
        Administrator,
        Manager,
        Root
    }
}
