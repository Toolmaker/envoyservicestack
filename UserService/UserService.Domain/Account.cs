﻿using System;
using AccountService.Core;
using AccountService.Domain.Exceptions;
using AccountService.Domain.Interfaces;

namespace AccountService.Domain
{
    public class Account : IAccount
    {
        private readonly IHashPassword _passwordHasher;
        private readonly ITimeProvider _timeProvider;
        private readonly IAccountSettings _accountSettings;

        public string Name { get; set; } 
        public string Email { get; set; } 
        public string Password { get; set; }
        public AccountRoles Role { get; set; }
        public DateTimeOffset LastLogin { get; private set; } 
        public int FailedLoginAttempts { get; private set; }
        public DateTimeOffset LastFailedLogon { get; private set; }
        public bool Locked { get; private set; } 
        public DateTimeOffset? LockedUntil { get; private set; }

        public Account(IHashPassword passwordHasher, ITimeProvider timeProvider,  IAccountSettings accountSettings)
        {
            _passwordHasher = passwordHasher;
            _timeProvider = timeProvider;
            _accountSettings = accountSettings;
        }

        private bool HasLockTimedOut => _timeProvider.Now >= LockedUntil;

        public void SetPassword(string password)
        {
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (string.IsNullOrEmpty(password)) throw new InvalidPasswordException();

            Password = _passwordHasher.Hash(password);
        }

        public bool ValidatePassword(string password)
        {
            if (Locked)
            {
                if (!HasLockTimedOut)
                {
                    return false;
                }

                Unlock();
            }

            var success = _passwordHasher.Hash(password) == Password;
            if (!success)
            {
                LoginFailed();
            }
            else
            {
                LoginSucceeded();
            }
            
            return success;
        }

        private void LoginSucceeded()
        {
            ResetFailedLoginAttempts();
            LastLogin = _timeProvider.Now;
        }

        private void ResetFailedLoginAttempts()
        {
            FailedLoginAttempts = 0;
        }

        private void LoginFailed()
        {
            if (_timeProvider.Now >= LastFailedLogon + _accountSettings.WrongPasswordTimeout)
            {
                ResetFailedLoginAttempts();
            }

            FailedLoginAttempts++;
            LastFailedLogon = _timeProvider.Now;

            if (FailedLoginAttempts < _accountSettings.MaxNumberOfFailedAttempts)
            {
                return;
            }

            Lock();
        }

        public void Lock()
        {
            if (Locked) return;

            Locked = true;
            LockedUntil = _timeProvider.Now + _accountSettings.LockoutDuration;
        }

        public void Unlock()
        {
            Locked = false;
            LockedUntil = DateTimeOffset.MinValue;
        }
    }
}