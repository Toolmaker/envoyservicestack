﻿using System;
using AccountService.Core;

namespace AccountService.Infrastructure
{
    public class TimeProvider : ITimeProvider
    {
        public DateTimeOffset Now => DateTimeOffset.Now;
    }
}
