﻿using System.Security.Cryptography;
using System.Text;
using AccountService.Core;

namespace AccountService.Infrastructure
{
    public class Sha256Hasher : IHashPassword
    {
        public string Hash(string input)
        {
            using (var hasher = SHA256.Create())
            {
                var hash = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));
                return System.Convert.ToBase64String(hash);
            }
        }
    }
}
