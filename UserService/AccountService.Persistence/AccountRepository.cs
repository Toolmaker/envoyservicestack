﻿using AccountService.Domain.Interfaces;
using AccountService.Domain.Interfaces.Repositories;
using Optional;
using System.Linq;
using AccountService.Core;
using AccountService.Domain;
using AutoMapper;

namespace AccountService.Persistence
{
    public class AccountRepository : IAccountRepository
    {
        private readonly AccountContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IServiceResolver _serviceResolver;

        public AccountRepository(AccountContext dbContext, IMapper mapper, IServiceResolver serviceResolver)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _serviceResolver = serviceResolver;
        }

        public Option<IAccount> GetByEmailAddress(string email)
        {
            var result = _dbContext.Accounts.SingleOrDefault(a => a.Email == email);
            if (result == null)
                return Option.None<IAccount>();

            var account = _serviceResolver.Resolve<Account>();
            return Option.Some<IAccount>(_mapper.Map<AccountEntity, Account>(result, account));
        }
    }
}
