﻿using AccountService.Domain;
using AutoMapper;

namespace AccountService.Persistence.Mappings
{
    public class PersistenceProfile : Profile
    {
        public PersistenceProfile()
        {
            CreateMap<AccountEntity, Account>();
        }
    }
}
