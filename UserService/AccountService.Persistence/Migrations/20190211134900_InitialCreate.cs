﻿using System;
using System.Security.Cryptography;
using System.Text;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AccountService.Persistence.Migrations
{
    public partial class InitialCreate : Migration
    {
        
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Email = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    LastLogin = table.Column<DateTimeOffset>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    Role = table.Column<int>(nullable: false),
                    FailedLoginAttempts = table.Column<int>(nullable: false),
                    LastFailedLogon = table.Column<DateTimeOffset>(nullable: true),
                    LockedUntil = table.Column<DateTimeOffset>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Email);
                });

            var password = Convert.ToBase64String(SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes("password")));

            migrationBuilder.InsertData(
                table: "Accounts",
                columns: new[]
                {
                    "Email", "Name", "Password", "LastLogin", "Locked", "Role", "FailedLoginAttempts",
                    "LastFailedLogon", "LockedUntil"
                },
                values: new object[]
                {
                    "patrick@80db.nl", "Patrick", password, DateTimeOffset.Now, false, "Administrator", 0, null, null
                });
        }

        //public string Email { get; set; }
        //public string Name { get; set; }
        //public string Password { get; set; }
        //public DateTimeOffset LastLogin { get; set; }
        //public bool Locked { get; set; }
        //public int Role { get; set; }
        //public int FailedLoginAttempts { get; set; }
        //public DateTimeOffset? LastFailedLogon { get; set; }
        //public DateTimeOffset? LockedUntil { get; set; }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Accounts");
        }
    }
}
