﻿using Microsoft.EntityFrameworkCore;

namespace AccountService.Persistence
{
    public class AccountContext : DbContext
    {
        public AccountContext(DbContextOptions<AccountContext> options) 
            : base(options)
        {
        }

        public DbSet<AccountEntity> Accounts { get; set; }
    }
}
