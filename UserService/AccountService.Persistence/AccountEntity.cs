﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AccountService.Persistence
{
    public class AccountEntity
    {
        [Key]
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public DateTimeOffset LastLogin { get; set; }
        public bool Locked { get; set; }
        public int Role { get; set; }
        public int FailedLoginAttempts { get; set; }
        public DateTimeOffset? LastFailedLogon { get; set; }
        public DateTimeOffset? LockedUntil { get; set; }
    }
}
